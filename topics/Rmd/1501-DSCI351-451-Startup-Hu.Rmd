---
title: "Setup Git repo from Rstudio"
author: "Yang Hu"
date: "Wednesday, January 14, 2015"
output:
  html_document:
    toc: yes
  pdf_document:
    number_sections: yes
    toc: yes
---

## Step 1: Set up Bitbucket account, accept invitation 

After setup your own Bitbucket account, you will receive an e-mail from Yang Hu, invite you to join the team "cwrudsci". Accept this invitation will give you access to a repository named "15s-dsci351-451-UG/GS-case ID".

![alt text](./figs/email.png)

This repository was forked from the main repo for this class "15s-dsci351-451-prof", it can be used as your personal version of the course repo. Your edits to this repo will not be seen by other students. 

## Step 2: Copy the URL of your repo

![alt text](./figs/bitbucket_repo.png) 

Copy the URL of you own repo from Bitbucket

## Step 3: Creat an empty folder

Create an empty folder under H:/Git, such as "DSCI351-451-case ID". This folder will be used to host you local copy of the repo.


![alt text](./figs/gitdirect.png) 

## Step 4: Open Rstudio from your desktop

Check whether your Rstudio is connected with git correctly. In Rstudio, click "Tools -> Global Options -> Git/SVN". Point "Git Executable" to the following directory "C:/Program Files (x86)/Git/bin/git.exe", click apply and OK. 

![alt text](./figs/R-gitconnection.png)

On the right top corner of Rstudio, click "New Project". 
-------------------------------------------------------
![alt text](./figs/R-newproject.png)

Choose the third button "Version Control" -> "Git". Copy the URL from Bitbucket. Change the folder name to what you just created for the empty folder. Point the directory to "H:/Git".
-----------------------------
![alt text](./figs/R-gitrepo.png)

You should get another window ask for your password for bitbucket. Then start cloning your repo from bitbucket to your local directory.
-------------------------
![alt text](./figs/R-gitclone.png)

Your project should have "Git" button. The next time you open Rstudio, choose "Open a project" then open the ".Rproj" file in your local git folder.
--------------------------------------
![alt text](./figs/R-project.png)

## Step 5: Manage your local Git repo with Git bash

Open Git bash from your desktop(do a search if not on the desk top). Git bash is a command line console just work as bash in Linix OS. 
Some useful command including:

Navigate to the Home Directory (Default folder for the current user):
```
$ cd
```

Navigate to a specific folder in the file system:
```
$cd/h/Git/DSCI351-451-ta
```

Go back to the previous Location:
```
$ cd ..
```

List the contents of the current directory (folder): 
```
$ ls
```

Create a folder in the current directory (without spaces in the folder name): 
```
$ mkdir NewFolderName
```

Create files required in the current directory to perform version control:
```
$ git init
```

Add all changes to tracked files and all new files to the next commit, but do not add file deletions: 
```
$ git add .
```

Commits all files changed since last commit. Does not include new files. 
```
$ git commit -a -m "Message Text"
```

Fetch changes from the specified branch in the remote, and merge them into the current local branch: 
```
$ git pull
```

Update the remote server with commits for all existing branches common to both the local system and the server. Branches on the local system which have never been pushed to the server are not shared. 
```
$ git push 
```
More basic bash command syntax go to: 
[Basic Git Command](http://www.codeproject.com/Articles/457305/Basic-Git-Command-Line-Reference-for-Windows-Users)