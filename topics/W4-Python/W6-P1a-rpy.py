### installing rpy ###
### in linux terminal execute following commands
##sudo pip install rpy2
### alternate ways 
##easy_install rpy2
##conda install rpy2
### launch python in terminal typing python
python

### inside python import the necessary libraries
import numpy as np
import scipy as sp
import pandas as pd
from rpy2.robjects.packages import importr ## will be used for import functions from r
import rpy2.robjects as ro ## will be used for handling r objects
import pandas.rpy.common as com ## common dataframe 


### import statistical packages from R
stats = importr('stats')
base = importr('base')
datasets = importr('datasets')

### import r dataframe to pandas
#ro.r('data(mtcars)') ## pass r command in r to load mtcars data
df  = com.load_data('mtcars')
df.head()
df.describe()
df['cyl:hp']=df.cyl*df.hp
df.head(2)

### convert python dataframe to R dataframe
rdf = com.convert_to_r_dataframe(df)

### run linear regression in R from python
formula = 'mpg ~ wt + cyl'
fit_full = stats.lm(formula, data=rdf)
print(base.summary(fit_full))




